---
layout: page
title: About
permalink: /about/
redirect_to:
    - https://community.amplify.aws/weekly-digest/about/
---

I collect as many community updates around the AWS Mobile ecosystem as I can, releasing them as a blog post once a week.  Feel free to [follow me on Twitter](https://twitter.com/FizzyInTheHall) to receive notifications of new blog posts and community updates.

The AWS Mobile ecosystem covers the following AWS services & developer community projects:

* [AWS Amplify Console](https://aws.amazon.com/amplify/console/)
* [Amplify Framework](https://amplify.aws)
* [AWS AppSync](https://aws.amazon.com/appsync)
* [AWS Device Farm](https://aws.amazon.com/devicefarm)

In addition, I include relevant information about other services (like Amazon DynamoDB, Amazon Cognito, Amazon Pinpoint, Amazon API Gateway and AWS Lambda) if the content is relevant to mobile developers.  I also include information about AWS SDKs, including [Amplify for JavaScript](https://aws.github.io/aws-amplify) and the native AWS SDKs for [Android](https://aws-amplify.github.io/docs/android/start) and [iOS](https://aws-amplify.github.io/docs/ios/start).

If you wish to submit a community update, drop me [an email](mailto:mobilequickie@gmail.com).
