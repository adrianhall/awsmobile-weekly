---
layout: post
title:  "Issue 31"
date:   2019-01-28 06:20:20 -0700
redirect_to:
    - https://community.amplify.aws/weekly-digest
---

Welcome to the AWS Mobile Weekly - a weekly roundup of the articles, podcasts, and videos that are relevant to developers who utilize the AWS platform.

> Interested in GraphQL?  Checkout [GraphQL Patterns](https://graphqlpatterns.simplecast.fm/episodes), a new podcast by [Nader Dabit], [Jimmy Jia], and [Mikhail Novikov].

## Meetups Webinars & Conferences

* [Building Offline-first GraphQL Applications with React & AWS AppSync](https://www.meetup.com/ReactJS-Austin-Meetup/events/257383017/) with [Nader Dabit] at 7pm on January 28, in Austin, TX 
* [Unify Your Data With AWS AppSync](https://www.meetup.com/AWS-AUS/events/qrfwlqyzcbnc/) with Kate Lanyon at 6pm on January 30, in Melbourne, Australia
* [GraphQL Contributor Day](https://www.thisdot.co/events/graphql-contributor-day) with [Rohan Deshpande] at 12pm on February 8, in San Francisco, CA
* Mobile App Hackathon (with prizes!) with [Dennis Hills] on February 22 at [DeveloperWeek 2019](https://www.eventbrite.com/e/developerweek-2019-tickets-47449325209?aff=estw) in San Francisco, CA
* [Building Full Stack Mobile Apps w/ React Native & GraphQL](https://www.meetup.com/Mobile-Romandie-Beer/events/256369167/) with [Nader Dabit] at 6:30pm on February 28, in Lausanne, Switzerland
* [Building Full Stack GraphQL Applications with React Native & AWS AppSync](https://appjs.co/full-stack-graphql-app-with-react-native-and-aws-appsync/), a workshop with [Nader Dabit] on April 5, 2019 at [AppJS co](https://appjs.co) in Krakow, Poland
* [Build a Real-time Chat App with React, GraphQL, AWS AppSync, & AWS Amplify](https://www.react-europe.org/#day-2019-May-21) with [Nader Dabit] and [Richard Threlkeld] at React Europe, May 23-24, 2019, in Paris, France

## AWS Services

* Blog: [Build and publish a Jekyll powered blog easily with AWS Amplify](https://medium.com/@FizzyInTheHall/build-and-publish-a-jekyll-powered-blog-easily-with-aws-amplify-529852042ab6) via [Adrian Hall]
* Blog: [Streaming Data With Amazon’s Oldest Cloud Service](https://itnext.io/streaming-data-with-amazons-oldest-cloud-service-51a6be142c8e) via [Dennis Hills]
* Visual: [AWS AppSync Managed GraphQL: A Visual Guide](https://www.awsgeek.com/posts/AWS-AppSync/) via [Jerry Hargrove]

## Mobile & Web Development

* Sample: [Sparta and Amplify](https://github.com/mweagle/SpartaAmplify) via [Matt Weagle]
* Sample: [AWS Appsync Chat for React](https://github.com/aws-samples/aws-appsync-chat-starter-react) updated with AWS Amplify Console, via [Ed Lima]
* Blog: [Amplify & Gatsby](http://jeffca.me/amplify/) via [Jeff C.]
* Blog: [Building a chat application using AWS AppSync and Serverless](https://serverless.com/blog/building-chat-appliation-aws-appsync-serverless/) via [Alex DeBrie]
* Blog: [Contrived React ToDo App Example with AWS AppSync and AWS Amplify](https://www.fullsapps.com/2019/01/aws-amplify-with-graphql-api-aws.html) via [Peter Dyer]
* Blog: [Going serverless with React and AWS Amplify: Development Environment Set up](https://medium.freecodecamp.org/going-serverless-with-react-and-aws-amplify-development-environment-set-up-9b15c3363bd) via [Peter Mbanugo]
* Blog: [Building Meetup Softball, part 1: Infrastructure](https://medium.com/@vienchitang/building-meetup-softball-part-1-infrastructure-75279e8db6a) via [Vien Tang]

Have an update, content, conference session, meetup or anything else to share with the community?  [Send it to me](mailto:photoadrian@outlook.com) for inclusion in this digest of the AWS Mobile happenings.

{% include contributors.md %}
