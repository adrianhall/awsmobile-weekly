---
layout: post
title:  "Issue 4"
date:   2018-07-02 06:20:20 -0700
redirect_to:
    - https://community.amplify.aws/weekly-digest
---

Have an update, content, conference session, meetup or anything else to share with the community?  [Send it to me](mailto:photoadrian@outlook.com) for inclusion in this digest of the AWS Mobile happenings.

# AWS AppSync

* Blog: [Subscriptions for React-Native with Appsync and Lambda Resolvers](https://medium.com/@st3f4n.s/subscriptions-for-react-native-with-appsync-and-lambda-resolvers-d53349a0755b) via [Stefan Star]
* Blog: [AppSync, S3, Data Pipeline & DynamoDB](https://medium.com/@ashleywnj/appsync-s3-data-pipeline-dynamodb-854f99d70b41) via [@Ashley_J_W](https://twitter.com/Ashley_J_W)
* Blog: [Use AWS GraphQL for React Native Message App](http://sandny.com/2018/06/28/create-aws-graphql-message-app/) via [Sandaruwan Nanayakkara].
* Podcast: [Diving into AWS AppSync](http://pca.st/oFFb) via [@AWSCloud]
* Video: [Building an AppSync + React + Amplify Frontend](https://www.youtube.com/watch?v=gzY1ql09_dE) via [Marcia Villalba]

# Native Development

* Blog: [Connect an Android Kotlin App to the cloud with AWS AppSync and Android Architecture Components](https://medium.com/@FizzyInTheHall/connect-an-android-kotlin-app-to-the-cloud-with-aws-appsync-and-android-architecture-components-b36deaafe52c?source=search_post) via [Adrian Hall]
* Blog: [Cloning Dark Mode List using React Native and AWS AppSync](https://medium.com/react-native-training/cloning-dark-mode-list-using-react-native-and-aws-appsync-1c0f7ff0d81e) via [Akshay Kadam]
* Meetup: [Building Cloud Enabled React Native Apps on AWS](https://www.meetup.com/Portland-ReactJS/events/251930855/) hosted by Portland ReactJS on July 10th @ 6:00pm
* Sample: [React Native Enterprise Social Messaging App](https://github.com/dabit3/heard) via [Nader Dabit]
* Video: [Build a YouTube aggregator from scratch with React Native, GraphQL, AWS Amplify & AWS AppSync](https://www.youtube.com/watch?v=GPxFaCMlFyc) via [Nader Dabit]

# Web Development

* Article: [AWS Amplify](https://searchaws.techtarget.com/definition/AWS-Amplify) via [Margaret Rouse]
* Blog: [Create a React ChatBot in 15 minutes](https://medium.com/@ednergizer/create-a-react-chatbot-in-15-minutes-3e614da8bce1) via [Ed Lima]
* Release: [AWS Amplify supports AI-powered chatbots with new Interactions category](https://aws.github.io/aws-amplify/media/interactions_guide) via [@AWSAmplify]
* Sample: [wolfeidau/cognito-vue-bootstrap](https://github.com/wolfeidau/cognito-vue-bootstrap) - a sample that demonstrates AWS Amplify, Cognito and Vue.js, via [Mark Wolfe]

{% include contributors.md %}