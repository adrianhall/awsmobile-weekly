---
layout: post
title:  "Issue 27"
date:   2018-12-17 06:20:20 -0700
redirect_to:
    - https://community.amplify.aws/weekly-digest
---

Welcome to the AWS Mobile Weekly - a weekly roundup of the articles, podcasts, and videos that are relevant to developers who utilize the AWS platform.

## Meetups Webinars & Conferences

* [Building serverless GraphQL applications with React & AWS AppSync](https://www.meetup.com/JSLouKY/events/256187785/) with [Nader Dabit] at 6pm on January 16, in Louisville, KY
* [The Future is All Front End](https://www.meetup.com/techlifecolumbus/events/dcvrcqyzcbvb/) with [Joe Emison] at 6:30pm on January 16, in Columbus, OH
* [Building Full Stack Mobile Apps w/ React Native & GraphQL](https://www.meetup.com/Mobile-Romandie-Beer/events/256369167/) with [Nader Dabit] at 6:30pm on February 28, in Lausanne, Switzerland

## AWS Services

* Video: [Deploying a React Application to AWS Using the Amplify Console](https://egghead.io/lessons/react-deploying-a-react-application-to-aws-using-the-amplify-console) via [Nader Dabit]
* Blog (Japanese): [How to use AWS Amplify CLI ~ From installation to initial setup](https://qiita.com/Junpei_Takagi/items/f2bc567761880471fd54) via [Junpei Takagi]

## Web Development

* Blog: [Multiple Serverless Environments with AWS Amplify](https://read.acloud.guru/multiple-serverless-environments-with-aws-amplify-344759e1be08) via [Nader Dabit]
* Workshop: [Build a Photo-Sharing Web App with AWS Amplify and AWS AppSync](https://amplify-workshop.go-aws.com/index.html) via [Gabe Hollombe]

I don't expect to have much content for the next two weeks due to a large portion of the world celebrating Christmas.  We'll be back in the New Year!

Have an update, content, conference session, meetup or anything else to share with the community?  [Send it to me](mailto:photoadrian@outlook.com) for inclusion in this digest of the AWS Mobile happenings.

{% include contributors.md %}