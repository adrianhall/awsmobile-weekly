<!-- Company Accounts -->
[@awscloud]: https://twitter.com/awscloud
[@AWSAmplify]: https://twitter.com/AWSAmplify
[@awsopen]: https://twitter.com/awsopen
[AWS re:Invent]: https://twitter.com/AWSreInvent
[Twitch TV]: https://twitch.tv/aws

<!-- AWS Staff -->
[Adrian Hall]: https://twitter.com/FizzyInTheHall
[AM Grobelny]: https://twitter.com/amsxbg
[Brice Pellé]: https://twitter.com/BricePelle
[Chris Munns]: https://twitter.com/chrismunns
[Craig Huber]: https://medium.com/@crhuber
[Danilo Poccia]: https://twitter.com/danilop
[Dennis Hills]: https://twitter.com/dmennis
[Ed Lima]: https://twitter.com/ednergizer
[Eng-Hwa Tan]: https://twitter.com/code4kopi
[Gabe Hollombe]: https://twitter.com/gabehollombe
[George Mao]: https://twitter.com/georgemao
[Heitor Lessa]: https://twitter.com/heitor_lessa
[Jane Shen]: https://twitter.com/j_shen
[Jerry Hargrove]: https://twitter.com/awsgeek
[Joshua Kahn]: https://twitter.com/joshuaakahn
[Kurt Kemple]: https://twitter.com/kurtiskemple
[Leo Drakopoulos]: https://twitter.com/mrleodrak
[Michael Labieniec]: https://twitter.com/mlabieniec
[Michael Paris]: https://twitter.com/mikeparisstuff
[Muhammad Ali]: https://twitter.com/mohdaliiqbal
[Nader Dabit]: https://twitter.com/dabit3
[Nikhil Dabhade]: https://twitter.com/in_nikoftime
[Nikhil Swaminathan]: https://twitter.com/TheSwaminator
[Nicki Klein]: https://twitter.com/nicki_23
[Otto Kruse]: https://twitter.com/ottokruse
[Richard Threlkeld]: https://twitter.com/undef_obj
[Rohan Deshpande]: https://twitter.com/appwiz
[Sindhoor Grandhi]: https://twitter.com/sindhoor27 
[Zach Barbitta]: https://twitter.com/ZachBarbitta

<!-- Community Members -->
[Akshay Kadam]: https://twitter.com/deadcoder0904
[Alain]: https://twitter.com/_idkjs
[Aldo Lugo]: https://twitter.com/aldomatic
[Alex Debrie]: https://twitter.com/alexbdebrie
[Alberto Jose Aragon Alvarez]: https://medium.com/@aragonalvarez
[Ana Narciso]: https://twitter.com/anahnarciso
[Anders Bbjoerne]: https://twitter.com/abjoerne
[Andreas Streichardt]: https://twitter.com/m0ppers
[Andrew Griffiths]: https://twitter.com/agriffonline
[Andrew Trigg]: https://twitter.com/AndrewSTrigg1
[Ashteya Biharisingh]: https://twitter.com/ashteya
[Arto Liukkonen]: https://twitter.com/artoliukkonen
[Bear Cahill]: https://twitter.com/brainofbear
[Ben Awad]: https://twitter.com/benawad97
[Brandon Willis]: https://twitter.com/ballenwillis
[Brad Pillow]: https://twitter.com/BradPillow
[Bruno Grácio]: https://github.com/bgracio
[Cory Schimmoeller]: https://twitter.com/coryschimm
[Dan Bruder]: https://twitter.com/danbruder
[Daniel Roy Greenfeld]: https://twitter.com/pydanny
[David Cizek]: https://twitter.com/dadc
[David Ramel]: https://twitter.com/dramel
[Defodgji Sogbohossou]: https://shinesolutions.com/author/defodjisogbohossou3618/
[Dennis Martin Herbers]: https://twitter.com/D2KX_
[Dhaval Nagar]: https://twitter.com/dhavaln
[Dhruv Kumar Jha]: https://twitter.com/dhruv_kumar_jha
[Erik Hanchett]: https://twitter.com/ErikCH
[Eytan Manor]: https://twitter.com/eytan_manor
[Fabien Mauqui]: https://twitter.com/fmauquie
[Faisal Hasnain]: https://twitter.com/faisalhasnainz
[Florian Motlik]: https://twitter.com/flomotlik
[Frikan Erwee]: https://medium.com/@ferwee
[Garrett Hogan]: https://twitter.com/ragtagthrone
[Gerard Gigliotti]: https://twitter.com/g_gigliotti
[Glen Bray]: https://medium.com/@glen.bray
[Glenn Grant]: https://twitter.com/_devalias
[Gyandeep Singh]: https://twitter.com/gyandeeps
[Hiroyuki Osaki]: https://twitter.com/Hiroyuki_OSAKI
[James Hamann]: https://twitter.com/jameshamann
[Jared Short]: https://twitter.com/ShortJared
[Jayakrishnan (JK)]: https://twitter.com/that_coder
[Jazz Tong]: https://github.com/jazztong
[Jean-Klaas Gunnink]: https://twitter.com/jgunnink
[Jeff C.]: https://twitter.com/jcar787
[Jeff Shillitto]: https://twitter.com/jeffshillitto
[Jesus Larrubia]: https://twitter.com/JesusLarrubia
[Jimmy Jia]: https://twitter.com/jimmy_jia
[Job Wiegant]: https://twitter.com/jwiegant
[Joe Emison]: https://twitter.com/JoeEmison
[Jon Gear]: https://twitter.com/geareduptech
[Junpei Takagi]: https://twitter.com/junpeirocks
[Jørgen Lybeck Hansen]: https://twitter.com/jorgenlybeck
[Kane Clover]: https://twitter.com/k_a_n__e
[Kevin Bell]: https://twitter.com/iamkevinbell
[Kuba Holak]: https://twitter.com/kubaholak
[Kyle Galbraith]: https://twitter.com/kylegalbraith
[Manoj Fernando]: https://twitter.com/mjmrz
[Marcia Villalba]: https://twitter.com/mavi888uy
[Margaret Rouse]: https://www.techtarget.com/contributor/Margaret-Rouse
[Mark Sergienko]: https://twitter.com/mkrn
[Mark Wolfe]: https://twitter.com/wolfeidau
[Matt Boyd]: https://www.linkedin.com/learning/instructors/matt-boyd
[Matt Netkow]: https://twitter.com/dotNetkow
[Mat Warger]: https://twitter.com/mwarger
[Matt Weagle]: https://twitter.com/mweagle
[Matteo Zuccon]: https://twitter.com/matteo_zuccon
[Matthias Biehl]: https://twitter.com/mattbiehl
[Maximiliano Contieri]: https://twitter.com/mcsee1
[Maxwell Russell]: https://twitter.com/MisteRussell
[Med Labouardy]: https://twitter.com/mlabouardy
[Michael Wittig]: https://twitter.com/hellomichibye
[Mike Smedley]: https://twitter.com/smdly
[Mikhail Novikov]: https://twitter.com/freiksenet
[Mohamed Labouardy]: https://twitter.com/mlabouardy
[Nathan Glover]: https://twitter.com/nathangloverAUS
[Ohsik Park]: https://twitter.com/Ohsik
[Peter Dyer]: https://twitter.com/peter_dyer
[Peter Mbanugo]: https://twitter.com/p_mbanugo
[Piotr Giedziun]: https://twitter.com/piotrgiedziun
[Prashanth HN]: https://twitter.com/prashanth
[Praveen Jayarajan]: https://twitter.com/praveenj1979
[Raoul Meyer]: https://dev.to/raoulmeyer
[Ryan Clark]: https://twitter.com/ryans140
[Ryan Marsh]: https://twitter.com/ryan_marsh
[Sam Goldstein]: https://twitter.com/_SamG
[Sandaruwan Nanayakkara]: https://sandy.com
[Sarjeel Yusuf]: https://twitter.com/SarjeelY
[Sebastian Müller]: https://twitter.com/sbstjn
[Shankar Raju]: https://twitter.com/snvishna
[Simran Rana]: https://medium.com/@simranrana7
[Soni Pandey]: https://twitter.com/sonipandey1
[Stefan Nagey]: https://twitter.com/stefan41
[Stefan Star]: https://twitter.com/stefan_star
[Steve Kinney]: https://twitter.com/stevekinney
[Steven Bryen]: https://twitter.com/steven_bryen
[Takeshi Amano]: https://medium.com/@takeshiamano
[Tetsushi Ito]: https://twitter.com/tetsushi_ito_
[Toan Phan]: https://medium.com/@toanphan
[Tobias Jonas]: https://twitter.com/jona7o
[Tom Bray]: https://twitter.com/tombray
[Ugur Arpaci]: https://dzone.com/users/2770058/ugurarpaci.html
[Victor Grenu]: https://twitter.com/zoph
[Vien Tang]: https://medium.com/@vienchitang
[Younes Henni]: https://twitter.com/YounesH1989
[Youssef Maghzaz]: https://twitter.com/ysfMag
[Zeb Girouard]: https://codepen.io/ZebGirouard/
